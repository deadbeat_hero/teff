<?php

namespace taroff\teff;

class TeaserEffSender
{
    private $socket;
    private $host;
    private $port;     // 6671

    public function __construct($host, $port)
    {
        $this->host = $host;
        $this->port = $port;
        $this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    }

    public function __destruct()
    {
        socket_close($this->socket);
    }

    public function send(Stat $stat)
    {
        $teasers = $stat->getTeaserPosCollection()->getAll();

        $message = pack('v', $stat->getCountryId());
        $message .= pack('L', $stat->getCategoryId());

        foreach ($teasers as $teaserPos) {
            $message .= pack('L', $teaserPos->teaserId) . pack('L', $teaserPos->position);
        }

        socket_sendto($this->socket, $message, strlen($message), 0, $this->host, $this->port);
    }
}
