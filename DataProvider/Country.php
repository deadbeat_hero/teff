<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\DataProvider;


class Country
{
    private $id;

    private $positions;

    public function __construct($id, $data)
    {
        $this->id = $id;

        $viewsSum = array_sum($data);

        foreach ($data as $position => $views) {
            $this->positions[$position] = $views/$viewsSum * 100;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPositions(): array
    {
        return $this->positions;
    }

    public function byPosition($number)
    {
        return $this->positions[$number];
    }

    public function topPosition() : int
    {
        return array_search(max($this->positions), $this->positions);
    }
}