<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\DataProvider;


class Category
{
    private $id;

    private $sliceByCountry;

    public function __construct($id, $data)
    {
        $this->id             = $id;
        $this->sliceByCountry = $data;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function forCountry($id): Country
    {
        if (!array_key_exists($id, $this->sliceByCountry)) {
            throw new \InvalidArgumentException('Category not found');
        }

        return new Country($id, $this->sliceByCountry[$id]);
    }

    public function bestPositions() : array
    {
        $countryIds = $this->getCountryList();

        $result = [];
        foreach ($countryIds as $countryId) {
            $country = $this->forCountry($countryId);
            $result[] = $country->topPosition();
        }

        return $result;
    }

    public function getCountryList(): array
    {
        return array_keys($this->sliceByCountry);
    }
}