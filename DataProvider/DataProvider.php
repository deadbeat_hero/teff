<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\DataProvider;


interface DataProvider
{
    const TYPE_SHORT = '10min';
    const TYPE_LONG  = '24h';

    public function asArray(): array;

    public function getMeta(): MetaInformation;

    public function onlyShowCaseInformation() : DataProvider;

    public function onlySiteInformation() : DataProvider;

    public function sortCategories(\Closure $callback);

    /**
     *   Есть ли статистика для тизера хотя бы по 1 стране
     * @return bool
     */
    public function isEmpty(): bool;


}