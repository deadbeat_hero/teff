<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\DataProvider;


class MetaInformation
{
    private $totalViews;
    private $viewsByCountry;

    public function __construct(array $metaData)
    {
        $this->totalViews = $metaData['totalViews'];
        $this->viewsByCountry = $metaData['totalViewsByCountry'];
    }

    public function asArray()
    {
        return [
            'totalViews' => $this->totalViews,
            'totalViewsByCountry' => $this->viewsByCountry
        ];
    }

    public function getTotalViews() : int
    {
        return $this->totalViews;
    }

    public function getViewsByCountryId($id) : int
    {
        if (array_key_exists($id, $this->viewsByCountry)) {
            return $this->viewsByCountry[$id];
        }

        throw new \InvalidArgumentException('Country not found');
    }

    public function isEmpty(): bool
    {
        return empty($this->viewsByCountry);
    }

}