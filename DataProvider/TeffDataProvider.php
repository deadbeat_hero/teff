<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\DataProvider;


class TeffDataProvider implements DataProvider
{
    const NEWS_CATEGORY_OFFSET = 100000;

    private $sliceByCategory = [];
    private $meta;

    public function __construct(array $data)
    {
        $this->meta = new MetaInformation($data['Meta']);

        foreach ($data['Data'] as $countryId => $country) {
            foreach ($country as $categoryId => $positions) {
                foreach ($positions as $position) {
                    reset($position);
                    $this->sliceByCategory[$categoryId][$countryId][key($position)] = $position[key($position)];
                }
            }
        }
    }

    public function asArray(): array
    {
        return $this->sliceByCategory;
    }

    public function forCategory($id): Category
    {
        if (!array_key_exists($id, $this->sliceByCategory)) {
            throw new \InvalidArgumentException('Category not found');
        }

        return new Category($id, $this->sliceByCategory[$id]);
    }

    public function sortCategories(\Closure $callback)
    {
        uksort($this->sliceByCategory, $callback);
    }

    public function hasCategory($id): bool
    {
        return array_key_exists($id, $this->sliceByCategory);
    }

    public function getMeta(): MetaInformation
    {
        return $this->meta;
    }

    public function getCategoryList(): array
    {
        return array_keys($this->sliceByCategory);
    }

    public function isEmpty(): bool
    {
        return $this->meta->isEmpty();
    }

    public function maxMinPosition(): array
    {
        $maxPosition = -1;
        $minPosition = PHP_INT_MAX;

        foreach ($this->sliceByCategory as $category) {
            foreach ($category as $country) {
                $position = array_keys($country)[0];
                if ($position > $maxPosition) {
                    $maxPosition = $position;
                }

                if ($position < $minPosition) {
                    $minPosition = $position;
                }
            }
        }

        if ($maxPosition === -1 || $minPosition === PHP_INT_MAX) {
            throw new \DomainException('Position not found');
        }

        return [$minPosition, $maxPosition];
    }

    public function onlyShowCaseInformation(): DataProvider
    {
        $instance = clone($this);

        $instance->sliceByCategory = array_filter($instance->sliceByCategory, function ($key) {
            return $key >= static::NEWS_CATEGORY_OFFSET;
        }, ARRAY_FILTER_USE_KEY);

        return $instance;
    }

    public function onlySiteInformation(): DataProvider
    {
        $instance = clone($this);

        $instance->sliceByCategory = array_filter($instance->sliceByCategory, function ($key) {
            return $key < static::NEWS_CATEGORY_OFFSET;
        }, ARRAY_FILTER_USE_KEY);

        return $instance;
    }


}