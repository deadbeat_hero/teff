## Info

Отправка информации о показах тизеров на позициях в TEFF сервис.
Получение информации о кол-ве показов тизера на определенных позициях на витрине из TEFF сервиса.


## Установка

``` composer require taroff/teff:* ```

## Tests
php vendor/phpunit/phpunit/phpunit tests

## Пример использования

```
<?php

require 'vendor/autoload.php';
use taroff\teff\Stat;
use taroff\teff\TeaserEffStat;
use taroff\teff\TeaserEffSender;


// отправка данных
$countryId = 1;
$stat = new Stat(new TeaserPositionCollection(), $countryId);

$data = [
    'positions' => [
        ['adId' => 15, 'position' => 24],
        ['adId' => 11, 'position' => 12],
    ]
];

if ($stat->load($data) && $stat->validate()) {
    $sender = new TeaserEffSender("localhost", 6671);
    $sender->send($stat);
}

// получение
$teffStat = new TeaserEffStat("127.0.0.1", 6671);
$teaserStat = json_decode($stat->getStatByTeaserId(TEASER_ID), true);
```

## Return format
{} - в случае отсутствия статы по тизеру


или json вида


{
  "Meta": {
      "totalViews":102205,
      "totalViewsByCountry": {
        "1":89718,
        "4":12487
      },

      // средняя взвешенная позиция по странам
      "wavgByCountry": {
        "1":15,
        "4":16
      }
  },
  "Data": {
    // countryId
    "1":[
      // position : views
      {"23":23975},
      {"5":16685},
      {"22":9181},
      {"20":5806},
      {"26":4716}
    ],
    "4":[
      {"4":11431},
      {"13":710},
      {"100":80},
      {"99":61},
      {"40":23}
    ]
  }
}