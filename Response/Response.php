<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\Response;


use taroff\teff\DataProvider\DataProvider;
use taroff\teff\DataProvider\TeffDataProvider;

class Response
{
    private $rawData;

    private $arrayData;

    public function __construct($rawData)
    {
        $this->arrayData = json_decode($rawData, true);
        if ($this->arrayData === null) {
            throw new \InvalidArgumentException('Received invalid json');
        }

        $this->rawData = $rawData;
    }

    public function createDataProvider($statisticType = null): DataProvider
    {
        if ($statisticType === null) {
            $teaserInfo = $this->asArray();
        } else {
            $teaserInfo = $this->asArray()[$statisticType];
        }

        return new TeffDataProvider($teaserInfo);
    }

    public function asRawData(): string
    {
        return $this->rawData;
    }

    public function asArray(): array
    {
        return $this->arrayData;
    }
}