<?php

namespace taroff\teff;

class TeaserPositionCollection
{
    private $data = [];

    public function add(TeaserPosition $teaserPos)
    {
        $this->data[$teaserPos->teaserId] = $teaserPos;
    }

    public function getAll()
    {
        return $this->data;
    }

    public function filter()
    {
    }

    public function skipInvalid()
    {
        foreach ($this->data as $teaserId => $teaserPos) {
            if (!$teaserPos->validate()) {
                unset($this->data[$teaserId]);
            }
        }
    }

    public function prepare()
    {
        $this->filter();
        $this->skipInvalid();
    }

    public function isEmpty()
    {
        return empty($this->data);
    }
}
