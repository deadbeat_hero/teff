<?php

namespace taroff\teff;

class Stat
{
    private $countryId;
    private $categoryId;
    private $teaserPosCollection;

    public function __construct($teaserPosCollection, $countryId = null, $categoryId = null)
    {
        $this->teaserPosCollection = $teaserPosCollection;
        $this->setCountryId($countryId);
        $this->categoryId = (int)$categoryId;
    }

    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    public function getCountryId()
    {
        return $this->countryId;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function filter()
    {
        $this->countryId = (int) $this->countryId;
    }

    public function validate()
    {
        $this->filter();
        if ($this->countryId < 1 || $this->countryId > 1000000) {
            return false;
        }

        return true;
    }

    public function getTeaserPosCollection()
    {
        $this->teaserPosCollection->prepare();
        return $this->teaserPosCollection;
    }

    /**
    * positions[N][adId]:1839542
    * positions[N][position]:8
    */
    public function load($params)
    {
        if (!isset($params['positions']) || !is_array($params['positions'])) {
            return false;
        }

        foreach ($params['positions'] as $data) {
            if (!is_array($data)) {
                continue;
            }

            if (!isset($data['adId']) || !isset($data['position'])) {
                continue;
            }

            $this->teaserPosCollection->add(
                new TeaserPosition($data['adId'], $data['position'])
            );
        }

        return !$this->teaserPosCollection->isEmpty();
    }


}
