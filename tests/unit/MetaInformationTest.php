<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\tests\unit;


use PHPUnit\Framework\TestCase;
use taroff\teff\DataProvider\DataProvider;
use taroff\teff\DataProvider\MetaInformation;

class MetaInformationTest extends TestCase
{
    private function createMetaInformation()
    {
        $json = '{"24h":{"Meta":{"totalViews":6,"totalViewsByCountry":{"1":6}},"Data":{"1":{"10":[{"1":3}],"5":[{"1":3}]}}}}';
        return new MetaInformation(json_decode($json, true)[DataProvider::TYPE_LONG]['Meta']);
    }

    public function testTotalViews()
    {
        $meta = $this->createMetaInformation();
        $this->assertEquals(6, $meta->getTotalViews());
    }

    public function testViewsByCountryId()
    {
        $meta = $this->createMetaInformation();
        $this->assertEquals(6, $meta->getViewsByCountryId(1));
    }
}