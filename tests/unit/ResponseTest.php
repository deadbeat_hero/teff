<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\tests\unit;


use PHPUnit\Framework\TestCase;
use taroff\teff\DataProvider\DataProvider;
use taroff\teff\Response\Response;

class ResponseTest extends TestCase
{
    private function createResponse()
    {
        return new Response('{"10min":{"Meta":{"totalViews":0,"totalViewsByCountry":{}},"Data":{}},"24h":{"Meta":{"totalViews":6,"totalViewsByCountry":{"1":6}},"Data":{"1":{"10":[{"1":3}],"5":[{"1":3}]}}}}');
    }

    public function testResponseAsArray()
    {
        $response = $this->createResponse();
        $this->assertInternalType('array', $response->asArray());
    }

    public function testResponseAsRawString()
    {
        $response = $this->createResponse();
        $this->assertInternalType('string', $response->asRawData());
    }

    public function testResponseAsDataProvider()
    {
        $response = $this->createResponse();
        $this->assertInstanceOf(DataProvider::class, $response->createDataProvider(DataProvider::TYPE_SHORT));
    }

    /** @expectedException \InvalidArgumentException */
    public function testInvalidResponse()
    {
        new Response('response');
    }
}