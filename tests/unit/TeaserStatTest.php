<?php

namespace taroff\teff\tests\unit;

use PHPUnit\Framework\TestCase;
use taroff\teff\TeaserStat;

/**
 */
final class TeaserStatTest extends TestCase
{
    private $stat;

    private function loadTestData()
    {
        $data = json_decode(
            '{"Meta":{"totalViews":23747,"totalViewsByCountry":{"1":20287,"4":3460}},"Data":{"1":[{"43":1034},{"40":1027},{"41":1019},{"44":1015},{"42":940}],"4":[{"19":343},{"21":324},{"17":296},{"20":291},{"16":271}]}}',
            true
        );

        $this->stat = new TeaserStat();
        $this->stat->load($data);
    }

    private function loadTestDataII()
    {
        $data = json_decode(
            '{"Meta":{"totalViews":20287,"totalViewsByCountry":{"1":20287}},"Data":{"1":[{"43":1034},{"40":1027},{"41":1019},{"44":1015},{"42":940}]}}',
            true
        );

        $this->stat = new TeaserStat();
        $this->stat->load($data);
    }

    private function loadTestDataIII()
    {
        $data = json_decode(
            '{"Meta":{"totalViews":25747,"totalViewsByCountry":{"1":20287,"4":3460,"1025": 2000}},"Data":{"1025":[{"6":1001,"5":999}], "1":[{"43":1034},{"40":1027},{"41":1019},{"44":1015},{"42":940}],"4":[{"19":343},{"21":324},{"17":296},{"20":291},{"16":271}]}}',
            true
        );

        $this->stat = new TeaserStat();
        $this->stat->load($data);
    }

    public function testTotalViews()
    {
        $this->loadTestData();
        $this->assertEquals($this->stat->getTotalViews(), 23747);
    }

    public function testCountries()
    {
        $this->loadTestData();
        $countries = $this->stat->getCountries();

        $this->assertContains(1, $countries);
        $this->assertContains(4, $countries);
        $this->assertEquals(count($countries), 2);
    }

    public function testCountriesViews()
    {
        $this->loadTestData();
        $this->assertEquals($this->stat->getTotalViewsByCountry(1), 20287);
        $this->assertEquals($this->stat->getTotalViewsByCountry(4), 3460);
    }

    public function testBestPosition()
    {
        $this->loadTestDataII();
        $this->assertContains(43, $this->stat->getBestPositions());
        $this->assertEquals(count($this->stat->getBestPositions()), 1);
    }

    public function testBestPositionII()
    {
        $this->loadTestData();
        $this->assertContains(43, $this->stat->getBestPositions());
        $this->assertContains(19, $this->stat->getBestPositions());
        $this->assertEquals(count($this->stat->getBestPositions()), 2);
    }

    public function testBestPositionIII()
    {
        $this->loadTestDataIII();
        $this->assertContains(43, $this->stat->getBestPositions());
        $this->assertContains(19, $this->stat->getBestPositions());
        $this->assertContains(6, $this->stat->getBestPositions());
        $this->assertEquals(count($this->stat->getBestPositions()), 3);
    }
}
