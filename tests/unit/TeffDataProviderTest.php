<?php
/**
 * Created by godzie@yandex.ru.
 * Date: 25.01.2018
 */

namespace taroff\teff\tests\unit;


use PHPUnit\Framework\TestCase;
use taroff\teff\DataProvider\Category;
use taroff\teff\DataProvider\Country;
use taroff\teff\DataProvider\DataProvider;
use taroff\teff\DataProvider\TeffDataProvider;

class TeffDataProviderTest extends TestCase
{
    private function getDataProvider()
    {
        $json = '{"10min":{"Meta":{"totalViews":0,"totalViewsByCountry":{}},"Data":{}},"24h":{"Meta":{"totalViews":6,"totalViewsByCountry":{"1":6}},"Data":{"1":{"10":[{"1":3}],"5":[{"1":3}]}}}}';
        return new TeffDataProvider(json_decode($json, true)[DataProvider::TYPE_LONG]);
    }

    public function testDataProviderAsArray()
    {
        $dataProvider = $this->getDataProvider();
        $this->assertInternalType('array', $dataProvider->asArray());
    }

    public function testDataProviderReturnCategory()
    {
        $dataProvider = $this->getDataProvider();
        $this->assertInstanceOf(Category::class, $dataProvider->forCategory(10));
    }

    public function testDataProviderReturnCategoryList()
    {
        $dataProvider = $this->getDataProvider();
        $this->assertEquals([10, 5], $dataProvider->getCategoryList());
    }

    /** @expectedException \InvalidArgumentException */
    public function testDataProviderErrorForNotExistCategory()
    {
        $dataProvider = $this->getDataProvider();
        $dataProvider->forCategory(100);
    }
}