<?php

namespace taroff\teff;

class TeaserPosition
{
    public $teaserId;
    public $position;

    public function __construct($teaserId, $position)
    {
        $this->teaserId = (int)$teaserId;
        $this->position = (int)$position;
    }

    public function filter()
    {
    }

    public function validate()
    {
        $this->filter();

        if ($this->teaserId < 1 || $this->teaserId > 4000000000) {
            return false;
        }

        if ($this->position < 1 || $this->position > 4000000000) {
            return false;
        }

        return true;
    }
}
