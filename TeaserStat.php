<?php

namespace taroff\teff;

/**
*   Загрузка статы 1 тизера
*/
class TeaserStat
{
    protected $data = [];
    protected $totalViews = 0;
    protected $totalViewsByCountry = [];

    /**
    *   Получение массива стран для которых есть стата
    *   @return array ID стран
    */
    public function getCountries()
    {
        return array_keys($this->totalViewsByCountry);
    }

    /**
    *   Получение массива топ1 позиций по всем странам
    *   @return array[countryId => position] with TOP1 positions by each country
    */
    public function getBestPositions()
    {
        $bestPositions = [];
        foreach ($this->data as $countryId => $data) {
            list($position, $views) = each($data);
            $bestPositions[$countryId] = $position;
        }

        return $bestPositions;
    }

    /**
    *   Получение кол-ва всех показов
    *   @return int
    */
    public function getTotalViews()
    {
        return (int)$this->totalViews;
    }

    /**
    *   Получение кол-ва показов по конкретной стране
    *   @param int $countryId
    *   @return int | null if countryId not found
    */
    public function getTotalViewsByCountry($countryId)
    {
        if (isset($this->totalViewsByCountry[$countryId])) {
            return $this->totalViewsByCountry[$countryId];
        }

        return null;
    }

    /**
    *   Есть ли статистика для тизера хотя бы по 1 стране
    *   @return bool
    */
    public function isEmpty()
    {
        return empty($this->totalViewsByCountry);
    }

    /**
    *   Кол-во стран для которых есть стата
    *   @return int
    */
    public function getCountryCount()
    {
        return count($this->totalViewsByCountry);
    }

    /**
    *   Получение ТОП позиций с процентами показов по ним по стране
    *   @param int $countryId
    *   @return array [posId: %views, ..., 'other' => процент по остальным позициям]
    */
    public function getPercentDataByCountry($countryId)
    {
        if (!isset($this->data[$countryId]) || !$this->getTotalViewsByCountry($countryId)) {
            return null;
        }

        $result = [];
        $percentSum = 0;
        foreach ($this->data[$countryId] as $position => $views) {
            $result[$position] = round(100 * $views / $this->getTotalViewsByCountry($countryId), 2);
            $percentSum += $result[$position];
        }

        if ($percentSum < 98) {
            $result['other'] = round(100 - $percentSum, 2);
        }

        return $result;
    }

    /**
    *   Получение ТОП позиций с кол-вом показов по ним по стране
    *   @param int $countryId
    *   @return array [posId: views, ...]
    */
    public function getDataByCountry($countryId)
    {
        if (isset($this->data[$countryId])) {
            return $this->data[$countryId];
        }

        return null;
    }

    /**
    *   Загрузка данных по тизеру их массива сформированого из json ответа TEFF сервиса
    *   @param array
    */
    public function load($data)
    {
        $this->loadInner($data);
    }

    private function arrayConvertToInt($array)
    {
        return array_map(function($val) {
            return intval($val);
        }, $array);
    }

    private function loadInner($data)
    {
        /* Пример статы в json
        {
          "Meta": {
              "totalViews":102205,
              "totalViewsByCountry": {
                "1":89718,
                "4":12487
              }
          },
          "Data": {
            // countryId
            "1":[
              // position : views
              {"23":23975},
              {"5":16685},
              {"22":9181},
              {"20":5806},
              {"26":4716}
            ],
            "4":[
              {"4":11431},
              {"13":710},
              {"100":80},
              {"99":61},
              {"40":23}
            ]
          }
        }
        */

        if (!is_array($data)) {
            return;
        }

        if (!isset($data['Meta']) || !isset($data['Data'])) {
            return;   
        }

        $this->setTotalViews($data);
        $this->setTotalViewsByCountry($data);
        $this->setData($data);
    }

    private function setTotalViews($data)
    {
        if (!isset($data['Meta']['totalViews'])) {
            return;
        }

        $this->totalViews = $data['Meta']['totalViews'];
    }

    private function setTotalViewsByCountry($data)
    {
        if (!isset($data['Meta']['totalViewsByCountry'])) {
            return;
        }

        $this->totalViewsByCountry = $data['Meta']['totalViewsByCountry'];
    }

    private function setData($data)
    {
        if (!isset($data['Data'])) {
            return;
        }

        foreach ($data['Data'] as $countryId => $data2) {
            foreach ($data2 as $index => $data3) {
                foreach ($data3 as $position => $views) {
                    $this->data[$countryId][$position] = $views;
                }
            }
        }
    }
}
