<?php

namespace taroff\teff;

use taroff\teff\Response\Response;

class TeaserEffStat
{
    const MAX_BYTE_READ = 1048576;

    const COMMAND_CLOSE = 0;
    const COMMAND_TEASER_STAT = 1;
    const COMMAND_TEASER_DELETE = 5;
    const COMMAND_SNAPSHOT = 10;
    const COMMAND_PRINT_TEASER = 20;
    const COMMAND_PRINT_TEASER_ALL = 21;
    const COMMAND_CLEAR_SHORT_STATISTIC = 22;

    private $socket;
    public $host;
    public $port;

    public function __construct($host = '127.0.0.1', $port = 6671)
    {
        $this->host = $host;
        $this->port = $port;
    }

    private function getSocket()
    {
        if (!$this->socket) {
            $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if (!$this->socket) {
                throw new \Exception('Can\'t create tcp socket!');
            }

            if (!socket_connect($this->socket, $this->host, $this->port)) {
                throw new \Exception('Can\'t connect to TeaserEffStat!');
            }
        }

        return $this->socket;
    }

    /**
    *   Получение всей инфы из сокета, в конце каждого сообщения стоит \n
    */
    private function readAll()
    {
        $result = '';
        while ($data = socket_read($this->getSocket(), self::MAX_BYTE_READ)) {
            $result .= $data;
            if (substr($data, -1) == "\n") {
                break;
            }
        }

        return $result;
    }

    public function getInfoAll()
    {
        $data = pack('C', self::COMMAND_PRINT_TEASER_ALL);
        socket_send($this->getSocket(), $data, strlen($data), 0);
        return ;
    }

    /**
     *   Получение информации по всем позициям по тизеру
     * @param int $teaserId
     * @return Response
     * @throws \Exception
     */
    public function getInfoByTeaserId($teaserId)
    {
        $data = pack('C', self::COMMAND_PRINT_TEASER) . pack('L', $teaserId);
        socket_send($this->getSocket(), $data, strlen($data), 0);
        return new Response($this->readAll());
    }

    /**
     *   Получение информации по ТОП5 позиций по тизеру
     * @param int $teaserId
     * @return json @see return format in README
     * @throws \Exception
     */
    public function getStatByTeaserId($teaserId)
    {
        $data = pack('C', self::COMMAND_TEASER_STAT) . pack('L', $teaserId);
        socket_send($this->getSocket(), $data, strlen($data), 0);
        return new Response($this->readAll());
    }

    public function clearShortStatistic($teaserId)
    {
        $data = pack('C', self::COMMAND_CLEAR_SHORT_STATISTIC) . pack('L', $teaserId);
        socket_send($this->getSocket(), $data, strlen($data), 0);
        return new Response($this->readAll());
    }

    /**
    *   Сделать снапшот в TEFF сервисе
    */
    public function snapshot()
    {
        $data = pack('C', self::COMMAND_SNAPSHOT);
        socket_send($this->getSocket(), $data, strlen($data), 0);
        return $this->readAll();
    }

    /**
     *   Удаление статы по тизеру
     * @return {"result": "Ok"} on success
     * @throws \Exception
     */
    public function clearByTeaserId($teaserId)
    {
        $data = pack('C', self::COMMAND_TEASER_DELETE) . pack('L', $teaserId);
        socket_send($this->getSocket(), $data, strlen($data), 0);
        return $this->readAll();
    }

    /**
    *   Закрытие соединения с TEFF сервисом
    */
    public function close()
    {
        if (null === $this->socket) {
            return;
        }

        socket_send($this->getSocket(), pack('C', self::COMMAND_CLOSE), 1, 0);
        socket_shutdown($this->getSocket());
        socket_close($this->getSocket());
    }

    public function __destruct()
    {
        $this->close();
    }
}
